#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

class DynamicArray
{
    public:
        DynamicArray() = default;
        DynamicArray(int size);
        ~DynamicArray();

        int getCapacity() const;
        int getArraySize() const;
        int getElementAt(int index) const;
        void pushElement(int value);
        void removeElementAt(int value);
        void dumpArray() const;

    private:

        int _capacity = 0;
        int _size = 0;
        int *_array = nullptr;
};

#endif /* DYNAMICARRAY_H */