#include <DynamicArray.h>
#include <iostream>
#include <stdexcept>

DynamicArray::DynamicArray(int capacity): _capacity(capacity), _array(new int[capacity])
{
}


DynamicArray::~DynamicArray()
{
    delete[] _array;
}

int DynamicArray::getCapacity() const
{
    return _capacity;
}

int DynamicArray::getArraySize() const
{
    return _size;
}


int DynamicArray::getElementAt(int index) const
{
    if (index > _size - 1)
    {
        throw std::length_error("Possition Invalid");
    }

    return _array[index];
}

void DynamicArray::pushElement(int value)
{
    if (_size == _capacity)
    {
        _capacity = (_capacity == 0) ? 4 : _capacity * 2 ;
        int *tmp = new int[_capacity];

        for (int i = 0; i < _size; i++)
        {
            tmp[i] = _array[i];
        }

        delete [] _array;
        _array = tmp;
        tmp = nullptr;
    }

    _array[_size] = value;
    _size++;
}

void DynamicArray::removeElementAt(int pos)
{
    if (pos > _size - 1)
    {
        throw std::length_error("Possition Invalid");
    }

    for (int _at = pos; _at < _size - 1; _at++)
    {
        _array[_at] = _array[_at + 1];
    }

    _size--;
}

void DynamicArray::dumpArray() const
{
    for (int i = 0; i < _size; i++)
    {
        std::cout << "array[" << i  << "]=" << _array[i] << "\n";
    }
}