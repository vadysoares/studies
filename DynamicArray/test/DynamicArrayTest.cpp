#include <gtest/gtest.h>
#include <DynamicArray.h>


namespace DynamicArrayTest
{

    class DynamicArrayTest : public testing::Test
    {
        protected:

            DynamicArray *ptrDynamicArray, *ptrDynamicArrayDefault;
            const int initialCapacity = 4;


            virtual void SetUp()
            {
                ptrDynamicArray = new DynamicArray(initialCapacity);
                ptrDynamicArrayDefault  = new DynamicArray();
            }

            virtual void TearDown()
            {
                delete ptrDynamicArray;
                delete ptrDynamicArrayDefault;
            }
    };

    TEST_F(DynamicArrayTest, TestpushOnDefaultCtor)
    {
        /*
         * Testing Test push and get:
         */
        EXPECT_EQ(0, ptrDynamicArrayDefault->getCapacity());
        EXPECT_EQ(0, ptrDynamicArrayDefault->getArraySize());
        ptrDynamicArrayDefault->pushElement(10); // 0
        EXPECT_EQ(4, ptrDynamicArrayDefault->getCapacity());
        ptrDynamicArrayDefault->pushElement(6); // 1
        ptrDynamicArrayDefault->pushElement(8); // 2
        ptrDynamicArrayDefault->pushElement(15); // 3
        EXPECT_EQ(4, ptrDynamicArrayDefault->getArraySize());
    }

    TEST_F(DynamicArrayTest, TestpushAndGet)
    {
        /*
         * Testing Test push and get:
         */
        EXPECT_EQ(4, ptrDynamicArray->getCapacity());
        ptrDynamicArray->pushElement(10); // 0
        ptrDynamicArray->pushElement(6); // 1
        ptrDynamicArray->pushElement(8); // 2
        ptrDynamicArray->pushElement(15); // 3
        EXPECT_EQ(4, ptrDynamicArray->getArraySize());
        EXPECT_EQ(10, ptrDynamicArray->getElementAt(0));
        EXPECT_EQ(6,  ptrDynamicArray->getElementAt(1));
        EXPECT_EQ(8,  ptrDynamicArray->getElementAt(2));
        EXPECT_EQ(15,  ptrDynamicArray->getElementAt(3));
    }

    TEST_F(DynamicArrayTest, Testresize)
    {
        /*
        * Testing resize:
        */
        ptrDynamicArray->pushElement(10); // 0
        ptrDynamicArray->pushElement(6); // 1
        ptrDynamicArray->pushElement(8); // 2
        ptrDynamicArray->pushElement(15); // 3
        ptrDynamicArray->pushElement(81); // 4
        EXPECT_EQ(81,  ptrDynamicArray->getElementAt(4));
        EXPECT_EQ(initialCapacity * 2, ptrDynamicArray->getCapacity());
        EXPECT_EQ(5, ptrDynamicArray->getArraySize());
    }

    TEST_F(DynamicArrayTest, TestRemoveAt)
    {
        /*
         * Testing removeAt:
         * Remove element _array[2] -> 8
         */
        ptrDynamicArray->pushElement(10); // 0
        ptrDynamicArray->pushElement(6); // 1
        ptrDynamicArray->pushElement(8); // 2
        ptrDynamicArray->removeElementAt(1);
        EXPECT_EQ(10, ptrDynamicArray->getElementAt(0));
        EXPECT_EQ(8, ptrDynamicArray->getElementAt(1));
        EXPECT_EQ(2, ptrDynamicArray->getArraySize());
    }


    TEST_F(DynamicArrayTest, TestThrow)
    {
        /*
         * Testing throw:
         */
        ASSERT_THROW(ptrDynamicArray->removeElementAt(4);, std::length_error);
        ASSERT_THROW(ptrDynamicArray->getElementAt(4);, std::length_error);
    }


}